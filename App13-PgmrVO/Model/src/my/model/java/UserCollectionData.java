package my.model.java;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UserCollectionData
{
  Iterator<Map<String, Object>> iter = null;

  public UserCollectionData(List<Map<String, Object>> data) {
      iter = data.iterator();
  }

  public Iterator<Map<String, Object>> getIterator() {
      return iter;
  }
}
