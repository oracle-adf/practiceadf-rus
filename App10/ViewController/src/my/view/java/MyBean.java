package my.view.java;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

public class MyBean {
    public MyBean() {
        super();
    }

    public void validateSalary(FacesContext facesContext, UIComponent uIComponent, Object object) {
        String clientId = uIComponent.getClientId(facesContext);
        boolean fatal = false;

        if ((object == null) || (object.toString().isEmpty())) {
            fatal = true;
        } else if (!isDigit(object.toString())) {
            fatal = true;
        } else if (Integer.parseInt(object.toString()) <= 0) {
            fatal = true;
        }

        if (fatal) {
            facesContext.addMessage(clientId,
                                    new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ошибка", "Заработная плата должна быть > 0"));
        }
    }

    private boolean isDigit(String st) {
        char[] utu = st.toCharArray();
        boolean isDigit = true;
        for (int i = 0; i < st.length(); i++) {
            if (!Character.isDigit(utu[i])) {
                isDigit = false;
                break;
            }
        }
        return isDigit;
    }

}
