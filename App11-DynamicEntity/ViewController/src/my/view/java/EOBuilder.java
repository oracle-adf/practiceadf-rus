package my.view.java;

import java.util.HashMap;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import my.model.AppModuleImpl;

import oracle.adf.controller.v2.lifecycle.Lifecycle;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.uicli.binding.JUCtrlAttrsBinding;
import oracle.jbo.uicli.binding.JUIteratorDef;
import oracle.jbo.uicli.mom.JUTags;


public class EOBuilder {

    private static final String DATACONTROL_NAME = "AppModuleDataControl";
    private RichPanelFormLayout form;

    public EOBuilder() {
        super();
    }

    public void setForm(RichPanelFormLayout form) {
        this.form = form;
    }

    public RichPanelFormLayout getForm() {
        return form;
    }

    private static ValueExpression resolveExpression(String expression) {
        FacesContext facesContext = JSFUtils.getFacesContext();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp = elFactory.createValueExpression(elContext, expression, Object.class);
        return valueExp;
    }


    public DCIteratorBinding getIterator(String iteratorName, String voName) {
        DCBindingContainer dcb = ADFUtils.getDCBindingContainer();

        //It could be created earlier
        DCIteratorBinding jub = dcb.findIteratorBinding(iteratorName);

        if (jub == null) {
            //Create and init an iterator binding definition
            JUIteratorDef jid = new JUIteratorDef(iteratorName, null, voName, null, 25);
            HashMap initValues = new HashMap();
            initValues.put(JUTags.DataControl, DATACONTROL_NAME);
            jid.init(initValues);
            //Create an iterator binding instance
            jub = jid.createIterBinding(BindingContext.getCurrent(), dcb);
            //Add the instance to the current binding container
            dcb.addIteratorBinding(iteratorName, jub);
        }
        return jub;
    }

    public void createUI() {
        if(form.getChildren().size()>0){
           // already created
            return;
        }
        DCBindingContainer dcb = ADFUtils.getDCBindingContainer();
        // Create Iterator - "voIterator"
        DCIteratorBinding itr = getIterator("voIterator", AppModuleImpl.DYNAMIC_VO_INSTANCE_INTERNAL);
        // Create Attrs Binding ...
        dcb.addControlBinding("EmployeeId", new JUCtrlAttrsBinding(null, itr, new String[] { "EmployeeId" }));
        dcb.addControlBinding("FirstName", new JUCtrlAttrsBinding(null, itr, new String[] { "FirstName" }));
        dcb.addControlBinding("LastName", new JUCtrlAttrsBinding(null, itr, new String[] { "LastName" }));
        // Create controls
        RichInputText rt = new RichInputText();
        String theExpression = "#{bindings.EmployeeId.inputValue}";
        rt.setId("rt1");
        rt.setLabel("EmployeeId");
        rt.setValueExpression("value", resolveExpression(theExpression));
        // add to parent form
        form.getChildren().add(rt);

        rt = new RichInputText();
        theExpression = "#{bindings.FirstName.inputValue}";
        rt.setId("rt2");
        rt.setLabel("FirstName");
        rt.setValueExpression("value", resolveExpression(theExpression));
        // add to parent form
        form.getChildren().add(rt);

        rt = new RichInputText();
        theExpression = "#{bindings.LastName.inputValue}";
        rt.setId("rt3");
        rt.setLabel("LastName");
        rt.setValueExpression("value", resolveExpression(theExpression));
        // add to parent form
        form.getChildren().add(rt);

        //Other controls goes here...
    }


    public boolean createEO() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("createVO");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            System.out.println(operationBinding.getErrors());
            return false;
        }
        return true;
    }


    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

   public void beforePhase(javax.faces.event.PhaseEvent phaseEvent) {        
        if (phaseEvent.getPhaseId() == PhaseId.RENDER_RESPONSE) {            
            if (createEO()) {
                createUI();
            }
        }
    }
}
