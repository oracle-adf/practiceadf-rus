package my.view.java;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.faces.event.ActionEvent;


public class UIMappingBean
{
  public UIMappingBean()
  {
    super();    
  }
  
  private Map<String, Object> map = new HashMap<String, Object>();

  public void setMap(Map map)
  {
    this.map = map;
  }

  public Map getMap()
  {
    return map;
  }
  
  public void printValues(ActionEvent actionEvent)
  {
    System.out.println(Arrays.toString(map.entrySet().toArray()));
  }
}
