create table MENU
(
  ID       NUMBER(10) not null,
  PID      NUMBER(10),
  NAME     VARCHAR2(50 CHAR),
  TASKFLOW VARCHAR2(250 CHAR)
)
;
-- Create/Recreate primary, unique and foreign key constraints 
alter table MENU
  add constraint PK_MENU primary key (ID)
;

insert into MENU (ID, PID, NAME, TASKFLOW)
values (1, null, '����������', null);
insert into MENU (ID, PID, NAME, TASKFLOW)
values (2, 1, '���������', null);
insert into MENU (ID, PID, NAME, TASKFLOW)
values (3, 1, '�����������', null);
commit;
