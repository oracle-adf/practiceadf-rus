package my.view.java;

import javax.faces.event.ActionEvent;

import my.util.ADFUtils;
import my.util.JSFUtils;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

public class PageBean {
    public PageBean() {
        super();
    }


    /**
     * Вызов с кнопки forEach
     * @param actionEvent
     */
    public void testForEach(ActionEvent actionEvent) {
        // вернет - null
        System.out.format("%s \r\n", JSFUtils.resolveExpression("#{row.attributeValues[2]}"));        
    }

    /**
     * Вызов с кнопки iterator
     * @param actionEvent
     */
    public void testIterator(ActionEvent actionEvent) {
        // вернет верный результат
        System.out.format("%s, %s \r\n", JSFUtils.resolveExpression("#{row.MinSalary}"), 
                          JSFUtils.resolveExpression("#{row.MaxSalary}"));
    }
}
