package my.view.java;

import java.sql.Timestamp;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.faces.event.ActionEvent;

import my.util.JSFUtils;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.model.AttributeCriterion;
import oracle.adf.view.rich.model.AttributeDescriptor;
import oracle.adf.view.rich.model.ConjunctionCriterion;
import oracle.adf.view.rich.model.Criterion;
import oracle.adf.view.rich.model.FilterableQueryDescriptor;


public class PageBean
{
  private RichTable table;

  public PageBean()
  {
    super();
  }

  public void setTable(RichTable table)
  {
    this.table = table;
  }

  public RichTable getTable()
  {
    return table;
  }


  /**
   * Получить критерий для атрибута 
   * @param cl
   * @param name
   * @return
   */
  private AttributeCriterion getAttrCriterionByName(List<Criterion> cl, String name)
  {
    AttributeCriterion result = null;
    for (Criterion ac: cl)
    {
      if (name.equals(((AttributeCriterion)ac).getAttribute().getName()))
      {
        result = (AttributeCriterion) ac;
        break;
      }
    }
    return result;
  }

  /**
   * Получить оператор сравнения по имени
   * @param ac
   * @param name
   * @return
   */
  private AttributeDescriptor.Operator getOperatorByName(AttributeCriterion ac, String name)
  {
    AttributeDescriptor.Operator result = null;
    Set<AttributeDescriptor.Operator> operators = ac.getAttribute().getSupportedOperators();
    for (AttributeDescriptor.Operator o: operators)
    {
      if (name.equals(o.getValue()))
      {
        result = o;
        break;
      }      
    }
    return result;
  }

  /**
   * Применить фильтр для даты
   * @param actionEvent
   */
  public void applyFilter(ActionEvent actionEvent)
  {
    FilterableQueryDescriptor queryDescriptor = (FilterableQueryDescriptor) getTable().getFilterModel();
    ConjunctionCriterion cc = queryDescriptor.getConjunctionCriterion();
    List<Criterion> cl = cc.getCriterionList();
    // получить критерий для атрибута
    AttributeCriterion ac = getAttrCriterionByName(cl, "HireDate");
    // получить оператор сравнения для атрибута
    AttributeDescriptor.Operator op = getOperatorByName(ac, "BETWEEN");
    // дата с: 
    Date hd1 = (Date) JSFUtils.getRequestAttribute("HireDate1");
    // дата по:
    Date hd2 = (Date) JSFUtils.getRequestAttribute("HireDate2");
    ac.setOperator(op);
    List list = ac.getValues();
    list.set(0, new Timestamp(hd1.getTime()));
    list.set(1, new Timestamp(hd2.getTime()));
    // послать событие фильтра 
    getTable().queueEvent(new QueryEvent(getTable(), queryDescriptor));
    // обновить таблицу
    AdfFacesContext.getCurrentInstance().addPartialTarget(getTable());
  }

}
