package my.view.model;

import java.util.ArrayList;
import java.util.List;

import my.view.java.BaseComponent;
import my.view.java.BaseComponentImpl;
import my.view.java.LovComponentImpl;
import my.view.java.LovItem;


public class PageBean
{
  public PageBean()
  {
    super();
    // Edit 
    items.add(new BaseComponentImpl("STRING", null, "Name", "Имя"));
    items.add(new BaseComponentImpl("STRING", null, "LastName", "Фамилия"));
    items.add(new BaseComponentImpl("NUMBER", null, "Age", "Возраст"));
    items.add(new BaseComponentImpl("DATE", null, "BirthDay", "День рождения"));    
    // LOV    
    List<LovItem> lovs = new ArrayList<LovItem>();
    lovs.add(new LovItem("Январь", 1 ));
    lovs.add(new LovItem("Февраль", 2 ));
    lovs.add(new LovItem("Март", 3 ));
    lovs.add(new LovItem("Апрель", 4 ));
    lovs.add(new LovItem("Март", 5 ));
    items.add(new LovComponentImpl(lovs, "LOV", null, "Month", "Месяц"));    
  }
    
  List<BaseComponent> items = new ArrayList<BaseComponent>();
  
  public List<BaseComponent> getItems()
  {
    return items;
  }
}
