package my.view.java;

public class LovItem
{
  
  private String label;
  private Object value;

  public LovItem(String label, Object value)
  {
    super();
    this.label = label;
    this.value = value;
  }

  public String getLabel()
  {
    return label;
  }

  public Object getValue()
  {
    return value;
  }
}
