package my.view.java;

import java.util.List;

public interface BaseComponent
{
  public String getDataType();
  public Object getValue();
  public String getName();
  public String getLabel();
  public List<LovItem> getLovs();
  
}
