package my.view.java;

import java.util.List;

public class LovComponentImpl extends BaseComponentImpl
{
  
  public LovComponentImpl(List<LovItem> lovs, String dataType, Object value, String name, String label)
  {
    super(dataType,  value,  name,  label);
    this.lovs = lovs;
  }
  
  private List<LovItem> lovs;
  
  @Override
  public List<LovItem> getLovs()
  {
   return lovs;  
  }
}
