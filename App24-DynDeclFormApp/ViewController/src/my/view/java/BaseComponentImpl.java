package my.view.java;

import java.util.List;

public class BaseComponentImpl implements BaseComponent
{
  
  // Тип данных
  private String dataType;
  // Значение
  private Object value;
  // Имя
  private String name;
  // Название
  private String label;

  public BaseComponentImpl(String dataType, Object value, String name, String label)
  {
    super();
    this.dataType = dataType;
    this.value = value;
    this.name = name;
    this.label = label;
  }


  public String getDataType()
  {
    return dataType;
  }

  public Object getValue()
  {
    return value;
  }

  public String getName()
  {
    return name;
  }

  public String getLabel()
  {
    return label;
  }

  public List<LovItem> getLovs()
  {
    return null;
  }
}
