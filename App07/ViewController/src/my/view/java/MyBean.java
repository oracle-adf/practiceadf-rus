package my.view.java;

import java.util.List;
import javax.faces.model.SelectItem;
import my.util.ADFUtils;
import oracle.adf.model.binding.DCIteratorBinding;

public class MyBean
{
  public MyBean()
  {
    super();
  }

  private List<SelectItem> items = null;

  public void initList()
  {
    getItems();
  }

  public List<SelectItem> getItems()
  {
    if (items == null)
    {
      items = ADFUtils.selectItemsForIterator("LocationsView1Iterator", "LocationId", "StreetAddress");
    }
    return items;
  }
}
