package practice.adf.view.java;

import java.io.IOException;

import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.naming.NamingException;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.FailedLoginException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import oracle.adf.controller.TaskFlowId;

import weblogic.servlet.security.ServletAuthentication;

import weblogic.security.URLCallbackHandler;
import weblogic.security.services.Authentication;

public class MainBean {
    private String login;
    private String password;
    private String taskFlowId = "/WEB-INF/task-flow-manager.xml#task-flow-manager";

    public MainBean() {
        super();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String doLogin() {
        try {
            if (login == null || password == null) {
                addMessage(FacesMessage.SEVERITY_WARN, "Login", "Укажите имя и пароль");
                return null;
            }

            String user = login.toLowerCase();
            byte[] pass = password.getBytes();

            FacesContext ctx = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();

            CallbackHandler handler = new URLCallbackHandler(user, pass);
            Subject mySubject = Authentication.login(handler);
            ServletAuthentication.runAs(mySubject, request);
            ServletAuthentication.generateNewSessionID(request);

            HttpServletResponse response = (HttpServletResponse)ctx.getExternalContext().getResponse();
            sendForward(request, response, "/adfAuthentication?success_url=/faces/index.jsf");

        } catch (FailedLoginException fle) {
            System.out.println(fle);
            addMessage(FacesMessage.SEVERITY_ERROR, "Login", fle.getMessage());
        } catch (Exception le) {
            System.out.println(le.getMessage());
            addMessage(FacesMessage.SEVERITY_ERROR, "Login", le.getMessage());
        }
        return null;
    }

    private void sendForward(HttpServletRequest request, HttpServletResponse response,
                             String forwardUrl) throws NamingException, SQLException {
        FacesContext ctx = FacesContext.getCurrentInstance();
        RequestDispatcher dispatcher = request.getRequestDispatcher(forwardUrl);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException se) {
            addMessage(FacesMessage.SEVERITY_ERROR, "Неожиданная ошибка во время логина", se.getMessage());
        } catch (IOException ie) {
            addMessage(FacesMessage.SEVERITY_ERROR, "Неожиданная ошибка во время логина", ie.getMessage());
        }
        ctx.responseComplete();
    }

    private void addMessage(FacesMessage.Severity sev, String caption, String message) {
        FacesMessage msg = new FacesMessage(sev, caption, message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }

    public String getTaskFlowId() {
        return taskFlowId;
    }

    public String getEmpDataSource() {
        if (taskFlowId.endsWith("manager")) {
            return "Manager1";
        } else {
            return "Employer1";
        }
    }

}
