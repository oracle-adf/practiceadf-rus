package my.java;

import java.math.BigDecimal;

import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.context.FacesContext;

import oracle.adf.view.rich.event.ActiveDataEntry;
import oracle.adf.view.rich.event.ActiveDataUpdateEvent;
import oracle.adf.view.rich.model.ActiveCollectionModelDecorator;
import oracle.adf.view.rich.model.ActiveDataModel;

import oracle.adfinternal.view.faces.activedata.ActiveDataEventUtil;

import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.SortableModel;


public class DepartmentManager extends ActiveCollectionModelDecorator implements IBackendListener
{
    private MyActiveDataModel _activeDataModel = new MyActiveDataModel();
    private CollectionModel _model = null;

    
    public CollectionModel getCollectionModel() {
       if (_model == null) {
         FacesContext ctx = FacesContext.getCurrentInstance();  
         ExpressionFactory ef = ctx.getApplication().getExpressionFactory();  
         ValueExpression ve = ef.createValueExpression(ctx.getELContext(), "#{DepartmentBackend}", DepartmentBackend.class);  
         DepartmentBackend context = (DepartmentBackend)ve.getValue(ctx.getELContext());  
          _model = new SortableModel(context.getDepartments());        
       }
       return _model;
     }

    public void onDeptartmentUpdate(Integer rowKey, String attribue, BigDecimal value)
    {
      
      if (rowKey != null) {
          System.out.println("changeEvent "+rowKey);  
          
      MyActiveDataModel asm = getMyActiveDataModel();
      
      // start the preparation for the ADS update
      asm.prepareDataChange();

      // create an ADS event, using an _internal_ util.....
      // this class is not part of the API
      ActiveDataUpdateEvent event = 
        ActiveDataEventUtil.buildActiveDataUpdateEvent(
        ActiveDataEntry.ChangeType.UPDATE, // type
        asm.getCurrentChangeCount(), // changeCount
        new Object[] {rowKey}, // rowKey
        null, //insertKey (null as we don't insert stuff;
        new String[] {attribue}, // attribue/property name that changes
        new Object[] {value}   // the payload for the above attribute
        );
      // deliver the new Event object to the ADS framework
      asm.notifyDataChange(event);
      }
    }

    public ActiveDataModel getActiveDataModel() {
         return _activeDataModel;
    }

    public MyActiveDataModel getMyActiveDataModel() {
        return _activeDataModel;
    }

}


