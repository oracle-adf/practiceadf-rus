package my.java;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

import my.model.vo.AppModuleImpl;

import my.util.ADFUtils;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;


public class DepartmentBackend
{
  public DepartmentBackend()
  {
  }

  private AppModuleImpl am;
  private String rowid = "AAAEATAAEAAAADNAAA";
  private Runnable dataChanger;
  private boolean isBusy = false;
  private Long id = 0L;

  public void setListener(IBackendListener listener)
  {
    this.listener = listener;
  }

  public IBackendListener getListener()
  {
    return listener;
  }

  private IBackendListener listener;

  private final List<Department> departments = new ArrayList<Department>();

  public List<Department> getDepartments()
  {
    return departments;
  }

  public void changeData(Integer rowKey, String attribute, BigDecimal salary)
  {
    // Add event code here...
    System.out.println("changeData");
    listener.onDeptartmentUpdate(rowKey,attribute, salary);
  }

  /**
   * Вычислить Salary и ID для измененной записи по ROWID
   * @param resultId
   * @return
   */
  private BigDecimal calcSalary()
  {
    BigDecimal salary = BigDecimal.valueOf(-1);
    try
    {
      InitialContext cxt = new InitialContext();
      String jndiName = "java:comp/env/jdbc/HRDS";
      DataSource dataSource = (DataSource) cxt.lookup(jndiName);
      Connection connection = dataSource.getConnection();
      // 
      PreparedStatement prepareStatement =
        connection.prepareStatement("select  department_id, rowid  from EMPLOYEES where rowid = ?");
      prepareStatement.setString(1, rowid);
      ResultSet rs = prepareStatement.executeQuery();
      if (rs.next())
      {
        Object depid = rs.getInt("department_id");
        System.out.println("depid:" + depid);
        prepareStatement.close();
        rs.close();

        prepareStatement =
            connection.prepareStatement("select  d.department_id, d.department_name, sum(e.salary) salary  from EMPLOYEES e join DEPARTMENTS d on e.department_id = d.department_id\n" +
              "where d.department_id= ? group by d.department_id, d.department_name");
        prepareStatement.setInt(1, (Integer) depid);
        rs = prepareStatement.executeQuery();
        if (rs.next())
        {
          salary = rs.getBigDecimal("salary");
          System.out.println("salary:" + salary);
          id = DepartmentBackend.this.getIdByDepart((Integer) depid);
          prepareStatement.close();
          rs.close();
        }
      }
      connection.close();
    }
    catch (NamingException e)
    {
      e.printStackTrace();
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    return salary;
  }

/**
 * Инициализировать список начальными данными
 * А также создать ждущий поток по отправки уведомления об изменении данных
 */
  @PostConstruct
  public void initData()
  {
    System.out.println("initData begin");
    am = (AppModuleImpl) ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
    ViewObject vo = am.findViewObject("DepartSalaryViewObj1");
    vo.executeQuery();
    Row row = vo.first();
    Long i = 0L;
    while (row != null)
    {
      departments.add(new Department((String) row.getAttribute("DepartmentName"), i,
                                     (Integer) row.getAttribute("DepartmentId"),
                                     (BigDecimal) row.getAttribute("Salary")));
      i++;
      row = vo.next();
    }
    System.out.println("initData end");

    dataChanger = new Runnable()
      {
        @Override
        public void run()
        {
          synchronized (DepartmentBackend.this)
          {
            try
            {
              System.out.println("wait");
              DepartmentBackend.this.wait();
              System.out.println("end wait");      
              // Вычислить Salary и ID в списке для измененной записи по ROWID
              BigDecimal salary = DepartmentBackend.this.calcSalary();
              DepartmentBackend.this.changeData(id.intValue(), "salary", salary);
            }
            catch (InterruptedException e)
            {
              System.out.println(e);
            }
          }
          DepartmentBackend.this.isBusy = false;
        }
      };

    System.out.println("start");
    // start the process
    Thread newThread = new Thread(dataChanger);
    newThread.start();
  }

  /**
   * Начать обновление данных в списке.
   * Сюда приходит вызов из DCNDemoListener (onDatabaseChangeNotification)
   * @param rowid
   */
  public void startUpdateProcess(String rowid)
  {
    System.out.println("startUpdateProcess:" + rowid);
    this.rowid = rowid;
    if (!isBusy)
    {
      synchronized (this)
      {
        isBusy = true;
        System.out.println("notify");
        this.notify();
        // start the process
        Thread newThread = new Thread(dataChanger);
        newThread.start();
      }
    }
  }

  private Long getIdByDepart(Integer depid)
  {
    List<Department> deps = getDepartments();
    for (Department dep: deps)
    {
      if (dep.getDepid().equals(depid))
      {
        return dep.getId();
      }
    }
    return 0L;
  }

}
