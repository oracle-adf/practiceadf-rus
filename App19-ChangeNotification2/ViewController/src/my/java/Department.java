package my.java;

import java.math.BigDecimal;

public class Department {
    public Department(String name, Long id, Integer depid, BigDecimal salary) {
        super();
        this.name = name;
        this.depid = depid;
        this.id = id;
        this.salary = salary;
    }

  protected String name;
  protected Long id;
  protected Integer depid;
  protected BigDecimal salary;


    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

  public void setSalary(BigDecimal Salary)
  {
    this.salary = Salary;
  }

  public BigDecimal getSalary()
  {
    return salary;
  }

  public void setDepid(Integer depid)
  {
    this.depid = depid;
  }

  public Integer getDepid()
  {
    return depid;
  }
}
