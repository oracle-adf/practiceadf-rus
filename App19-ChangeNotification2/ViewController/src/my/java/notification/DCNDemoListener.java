package my.java.notification;

import oracle.jdbc.dcn.DatabaseChangeEvent;
import oracle.jdbc.dcn.DatabaseChangeListener;
import oracle.jdbc.dcn.QueryChangeDescription;
import oracle.jdbc.dcn.RowChangeDescription;
import oracle.jdbc.dcn.TableChangeDescription;


public class DCNDemoListener
  implements DatabaseChangeListener
{
  DBChangeNotification dbChangeNotification;

  DCNDemoListener(DBChangeNotification dbChangeNotification)
  {
    this.dbChangeNotification = dbChangeNotification;
  }

  @Override
  public void onDatabaseChangeNotification(DatabaseChangeEvent databaseChangeEvent)
  {
    System.out.println("onDatabaseChangeNotification");
    QueryChangeDescription[] changeDescription = databaseChangeEvent.getQueryChangeDescription();

    //System.out.println("changeDescription:"+changeDescription);
    for (QueryChangeDescription change: changeDescription)
    {
      TableChangeDescription[] tdescription = change.getTableChangeDescription();
      for (TableChangeDescription tchange: tdescription)
      {
        RowChangeDescription[] rchange = tchange.getRowChangeDescription();
        for (RowChangeDescription rcd: rchange)
        {
          System.out.println("Affected ROWID -> " + rcd.getRowid().stringValue());
          dbChangeNotification.onDatabaseChange(rcd.getRowid().stringValue());
        }
      }

    }
  }
}
