package my.java.notification;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Properties;

import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.context.FacesContext;

import my.java.DepartmentBackend;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleStatement;
import oracle.jdbc.dcn.DatabaseChangeRegistration;


public class DBChangeNotification
{
  private OracleConnection conn;

  public DBChangeNotification(OracleConnection conn)
  {
    super();
    this.conn = conn;
  }

  private DepartmentBackend departmentBackend;
  private DatabaseChangeRegistration dcr;


  void run()
    throws SQLException
  {

    // first step: create a registration on the server:
    Properties prop = new Properties();

    // Ask the server to send the ROWIDs as part of the DCN events (small performance
    // cost):
    prop.setProperty(OracleConnection.DCN_NOTIFY_ROWIDS, "true");
    //Set the DCN_QUERY_CHANGE_NOTIFICATION option for query registration with finer granularity.
    prop.setProperty(OracleConnection.DCN_QUERY_CHANGE_NOTIFICATION, "true");

    // The following operation does a roundtrip to the database to create a new
    // registration for DCN. It sends the client address (ip address and port) that
    // the server will use to connect to the client and send the notification
    // when necessary. Note that for now the registration is empty (we haven't registered
    // any table). This also opens a new thread in the drivers. This thread will be
    // dedicated to DCN (accept connection to the server and dispatch the events to
    // the listeners).
    dcr = conn.registerDatabaseChangeNotification(prop);

    try
    {
      // add the listenerr:
      DCNDemoListener list = new DCNDemoListener(this);
      dcr.addListener(list);
      System.out.println("registration.");
      // second step: add objects in the registration:
      Statement stmt = conn.createStatement();
      // associate the statement with the registration:
      ((OracleStatement) stmt).setDatabaseChangeRegistration(dcr);      
      ResultSet rs = stmt.executeQuery("select employee_id, salary from EMPLOYEES where department_id > 10");
      while (rs.next())
      {
      }
      String[] tableNames = dcr.getTables();
      for (int i = 0; i < tableNames.length; i++)
        System.out.println(tableNames[i] + " is part of the registration.");
      rs.close();
      stmt.close();
      
      FacesContext ctx = FacesContext.getCurrentInstance();
      ExpressionFactory ef = ctx.getApplication().getExpressionFactory();
      ValueExpression ve =
        ef.createValueExpression(ctx.getELContext(), "#{DepartmentBackend}", DepartmentBackend.class);
      departmentBackend = (DepartmentBackend) ve.getValue(ctx.getELContext());
      
    }
    catch (SQLException ex)
    {
      // if an exception occurs, we need to close the registration in order
      // to interrupt the thread otherwise it will be hanging around.
      if (conn != null)
        conn.unregisterDatabaseChangeNotification(dcr);
      throw ex;
    }
    finally
    {
      try
      {
        // Note that we close the connection!
        conn.close();
      }
      catch (Exception innerex)
      {
        innerex.printStackTrace();
      }
    }

  }
  
  public void stop(OracleConnection conn)
  {
    try
    {
      conn.unregisterDatabaseChangeNotification(dcr);
    }
    catch (SQLException e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        conn.close();
      }
      catch (SQLException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  public void onDatabaseChange(String rowid)
  {
    System.out.println("onDatabaseChange");
    departmentBackend.startUpdateProcess(rowid); 
  }
}
