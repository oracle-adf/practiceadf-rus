package my.java.notification;

import java.sql.SQLException;

import javax.faces.event.ActionEvent;

import my.model.vo.AppModuleImpl;

import my.util.ADFUtils;

import oracle.jdbc.OracleConnection;


public class PageBean
{
 
    public PageBean()
    {
      super();
    }

   private DBChangeNotification notification;

    public void startNotification(ActionEvent actionEvent)
    {
      AppModuleImpl am = (AppModuleImpl) ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
      OracleConnection cn = (OracleConnection) am.getConnection();

      notification = new DBChangeNotification(cn);

      try
      {
        notification.run();
      }
      catch (SQLException e)
      {
        System.out.println(e);
      }
    }

  public void stopNatification(ActionEvent actionEvent)
  {
    AppModuleImpl am = (AppModuleImpl) ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
    OracleConnection cn = (OracleConnection) am.getConnection();
    notification.stop(cn);
  }
}
