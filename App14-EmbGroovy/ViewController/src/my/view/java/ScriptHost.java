package my.view.java;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import my.model.vo.AppModuleImpl;


public class ScriptHost
{
  private AppModuleImpl model;
  private AdfBindings adfBindings;
  private Binding binding;
  private GroovyShell shell;

  public ScriptHost(AppModuleImpl model, AdfBindings adfBindings)
  {
    super();    
    this.model = model;
    this.adfBindings = adfBindings;
    binding = new Binding();    
    // глобальная переменная - model  
    binding.setVariable("model", this.model);
    // глобальная переменная - bindings
    binding.setVariable("bindings", this.adfBindings);
    shell = new GroovyShell(binding);
  }

  public void evalScript(String script)
  {
    shell.evaluate(script);
  }
  
}
