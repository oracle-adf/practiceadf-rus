package my.view.java;

import javax.faces.event.ActionEvent;

import my.model.vo.AppModuleImpl;

import my.util.ADFUtils;

import oracle.jbo.Row;
import oracle.jbo.server.ViewObjectImpl;


public class MyBean
{
  public MyBean()
  {
    super();
  }

  private ScriptHost sc = null;
  private AdfBindings adfBindings = null;

  public void runScript(ActionEvent actionEvent)
  {
    AppModuleImpl am = (AppModuleImpl) ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
    if (sc == null)
    {
      // создаем скрипт хост с переменными (AppModuleImpl, AdfBindings)
      adfBindings = new AdfBindings();
      sc = new ScriptHost(am, adfBindings);
    }
    ViewObjectImpl vo = am.getScriptsView1();
    Row row = vo.getCurrentRow();
    // запускаем код. в атрибуте "Code" находится groovy код
    sc.evalScript((String) row.getAttribute("Code"));
  }
}
