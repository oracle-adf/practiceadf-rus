package my.view.java;

import javax.faces.event.ActionEvent;

import my.model.vo.EmployeesViewRowImpl;

import my.util.ADFUtils;

import oracle.jbo.ApplicationModule;
import oracle.jbo.ViewObject;

public class PageBean
{
  public PageBean()
  {
    super();
  }

  public void doIt(ActionEvent actionEvent)
  {
    ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
    ViewObject vo = am.findViewObject("EmployeesView1");
    EmployeesViewRowImpl row = (EmployeesViewRowImpl) vo.getCurrentRow();
    row.doIt();    
  }
}
