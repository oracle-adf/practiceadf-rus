/*@lineinfo:filename=MyCtx*/ /*@lineinfo:user-code*/ /*@lineinfo:1^1*/
package practice.adf.model.java;

import java.sql.SQLException;

import sqlj.runtime.ref.DefaultContext;
import sqlj.runtime.ConnectionContext;

import java.sql.Connection;

public class MyCtx {

    /* connection management */
    protected Connection __onn = null;
    protected javax.sql.DataSource __dataSource = null;

    public void setDataSource(javax.sql.DataSource dataSource) throws SQLException {
        release();
        __dataSource = dataSource;
    }

    public void setDataSourceLocation(String dataSourceLocation) throws SQLException {
        javax.sql.DataSource dataSource;
        try {
            Class cls = Class.forName("javax.naming.InitialContext");
            Object ctx = cls.newInstance();
            java.lang.reflect.Method meth = cls.getMethod("lookup", new Class[] { String.class });
            dataSource =
                    (javax.sql.DataSource)meth.invoke(ctx, new Object[] { "java:comp/env/" + dataSourceLocation });
            setDataSource(dataSource);
        } catch (Exception e) {
            throw new java.sql.SQLException("Error initializing DataSource at " + dataSourceLocation + ": " +
                                            e.getMessage());
        }
    }

    public Connection getConnection() throws SQLException {
        if (__onn != null)
            return __onn;
        else if (__tx != null)
            return __tx.getConnection();
        else if (__dataSource != null)
            __onn = __dataSource.getConnection();
        return __onn;
    }

    public void release() throws SQLException {
        if (__tx != null && __onn != null)
            __tx.close(ConnectionContext.KEEP_CONNECTION);
        __onn = null;
        __tx = null;
        __dataSource = null;
    }

    public void closeConnection() {
        if (__dataSource != null) {
            try {
                if (__onn != null) {
                    __onn.close();
                }
            } catch (java.sql.SQLException e) {
            }
            try {
                if (__tx != null) {
                    __tx.close();
                }
            } catch (java.sql.SQLException e) {
            }
            __onn = null;
            __tx = null;
        }
    }
    protected DefaultContext __tx = null;

    public void setConnectionContext(DefaultContext ctx) throws SQLException {
        release();
        __tx = ctx;
    }

    public DefaultContext getConnectionContext() throws SQLException {
        if (__tx == null) {
            __tx =
(getConnection() == null) ? DefaultContext.getDefaultContext() : new DefaultContext(getConnection());
        }
        return __tx;
    };

    /* constructors */

    public MyCtx() throws SQLException {
        __tx = DefaultContext.getDefaultContext();
    }

    public MyCtx(DefaultContext c) throws SQLException {
        __tx = c;
    }

    public MyCtx(Connection c) throws SQLException {
        __onn = c;
        __tx = new DefaultContext(c);
    }

    public MyCtx(javax.sql.DataSource ds) throws SQLException {
        __dataSource = ds;
    }

    public String getUser() throws java.sql.SQLException {
        String __jPt_result = null;
        try {
            /*@lineinfo:generated-code*/ /*@lineinfo:73^5*/

            //  ************************************************************
            //  #sql [getConnectionContext()] __jPt_result = { VALUES("MY_CTX".GET_USER())  };
            //  ************************************************************

            {
                // declare temps
                oracle.jdbc.OracleCallableStatement __sJT_st = null;
                sqlj.runtime.ref.DefaultContext __sJT_cc = getConnectionContext();
                if (__sJT_cc == null)
                    sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
                sqlj.runtime.ExecutionContext.OracleContext __sJT_ec =
                    ((__sJT_cc.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() :
                     __sJT_cc.getExecutionContext().getOracleContext());
                try {
                    String theSqlTS = "BEGIN :1 := \"MY_CTX\".GET_USER()  \n; END;";
                    __sJT_st = __sJT_ec.prepareOracleCall(__sJT_cc, "0practice.adf.view.java.MyCtx", theSqlTS);
                    if (__sJT_ec.isNew()) {
                        __sJT_st.registerOutParameter(1, oracle.jdbc.OracleTypes.VARCHAR);
                    }
                    // execute statement
                    __sJT_ec.oracleExecuteUpdate();
                    // retrieve OUT parameters
                    __jPt_result = (String)__sJT_st.getString(1);
                } finally {
                    __sJT_ec.oracleClose();
                }
            }


            //  ************************************************************

            /*@lineinfo:user-code*/ /*@lineinfo:73^84*/
        } catch (java.sql.SQLException _err) {
            try {
                getConnectionContext().getExecutionContext().close();
                closeConnection();
                if (__dataSource == null)
                    throw _err;
                /*@lineinfo:generated-code*/ /*@lineinfo:79^5*/

                //  ************************************************************
                //  #sql [getConnectionContext()] __jPt_result = { VALUES("MY_CTX".GET_USER())  };
                //  ************************************************************

                {
                    // declare temps
                    oracle.jdbc.OracleCallableStatement __sJT_st = null;
                    sqlj.runtime.ref.DefaultContext __sJT_cc = getConnectionContext();
                    if (__sJT_cc == null)
                        sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
                    sqlj.runtime.ExecutionContext.OracleContext __sJT_ec =
                        ((__sJT_cc.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() :
                         __sJT_cc.getExecutionContext().getOracleContext());
                    try {
                        String theSqlTS = "BEGIN :1 := \"MY_CTX\".GET_USER()  \n; END;";
                        __sJT_st = __sJT_ec.prepareOracleCall(__sJT_cc, "1practice.adf.view.java.MyCtx", theSqlTS);
                        if (__sJT_ec.isNew()) {
                            __sJT_st.registerOutParameter(1, oracle.jdbc.OracleTypes.VARCHAR);
                        }
                        // execute statement
                        __sJT_ec.oracleExecuteUpdate();
                        // retrieve OUT parameters
                        __jPt_result = (String)__sJT_st.getString(1);
                    } finally {
                        __sJT_ec.oracleClose();
                    }
                }


                //  ************************************************************

                /*@lineinfo:user-code*/ /*@lineinfo:79^84*/
            } catch (java.sql.SQLException _err2) {
                try {
                    getConnectionContext().getExecutionContext().close();
                } catch (java.sql.SQLException _sqle) {
                }
                throw _err;
            }
        }
        return __jPt_result;
    }

    public void setUser(String P_USERNAME) throws java.sql.SQLException {
        try {
            /*@lineinfo:generated-code*/ /*@lineinfo:93^5*/

            //  ************************************************************
            //  #sql [getConnectionContext()] { CALL "MY_CTX".SET_USER(
            //        :P_USERNAME)  };
            //  ************************************************************

            {
                // declare temps
                oracle.jdbc.OraclePreparedStatement __sJT_st = null;
                sqlj.runtime.ref.DefaultContext __sJT_cc = getConnectionContext();
                if (__sJT_cc == null)
                    sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
                sqlj.runtime.ExecutionContext.OracleContext __sJT_ec =
                    ((__sJT_cc.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() :
                     __sJT_cc.getExecutionContext().getOracleContext());
                try {
                    String theSqlTS = "BEGIN \"MY_CTX\".SET_USER(\n       :1  ) \n; END;";
                    __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc, "2practice.adf.view.java.MyCtx", theSqlTS);
                    // set IN parameters
                    __sJT_st.setString(1, P_USERNAME);
                    // execute statement
                    __sJT_ec.oracleExecuteUpdate();
                } finally {
                    __sJT_ec.oracleClose();
                }
            }


            //  ************************************************************

            /*@lineinfo:user-code*/ /*@lineinfo:94^20*/
        } catch (java.sql.SQLException _err) {
            try {
                getConnectionContext().getExecutionContext().close();
                closeConnection();
                if (__dataSource == null)
                    throw _err;
                /*@lineinfo:generated-code*/ /*@lineinfo:100^5*/

                //  ************************************************************
                //  #sql [getConnectionContext()] { CALL "MY_CTX".SET_USER(
                //        :P_USERNAME)  };
                //  ************************************************************

                {
                    // declare temps
                    oracle.jdbc.OraclePreparedStatement __sJT_st = null;
                    sqlj.runtime.ref.DefaultContext __sJT_cc = getConnectionContext();
                    if (__sJT_cc == null)
                        sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
                    sqlj.runtime.ExecutionContext.OracleContext __sJT_ec =
                        ((__sJT_cc.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() :
                         __sJT_cc.getExecutionContext().getOracleContext());
                    try {
                        String theSqlTS = "BEGIN \"MY_CTX\".SET_USER(\n       :1  ) \n; END;";
                        __sJT_st =
                                __sJT_ec.prepareOracleStatement(__sJT_cc, "3practice.adf.view.java.MyCtx", theSqlTS);
                        // set IN parameters
                        __sJT_st.setString(1, P_USERNAME);
                        // execute statement
                        __sJT_ec.oracleExecuteUpdate();
                    } finally {
                        __sJT_ec.oracleClose();
                    }
                }


                //  ************************************************************

                /*@lineinfo:user-code*/ /*@lineinfo:101^20*/
            } catch (java.sql.SQLException _err2) {
                try {
                    getConnectionContext().getExecutionContext().close();
                } catch (java.sql.SQLException _sqle) {
                }
                throw _err;
            }
        }
    }
}/*@lineinfo:generated-code*/