package my.view.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import oracle.adf.view.rich.render.ClientEvent;


public class Component
{
  public Component()
  {
    super();
    // тестовые данные
    items.add(new SelectItem(new Integer(1), "2000"));
    items.add(new SelectItem(new Integer(2), "2001"));
    items.add(new SelectItem(new Integer(3), "2002"));
    items.add(new SelectItem(new Integer(4), "2003"));
    items.add(new SelectItem(new Integer(5), "2004"));
    items.add(new SelectItem(new Integer(6), "2006"));
    items.add(new SelectItem(new Integer(7), "2007"));
    items.add(new SelectItem(new Integer(8), "2008"));
  }

  private List<SelectItem> items = new ArrayList<SelectItem>();

  public List<SelectItem> getItems()
  {
    return items;
  }

  /**
   * Снять выделение всех элементов
   */
  private void reset()
  {
    for (SelectItem item: items)
    {
      item.setNoSelectionOption(true);
    }
  }

  /**
   * Выделить элемент по ИД
   * @param value
   */
  private void selectByValue(Object value)
  {
    for (SelectItem item: items)
    {
      if (item.getValue().equals(((Double) value).intValue()))
      {
        item.setNoSelectionOption(false);
        System.out.println("select item:" + item.getValue());
      }
    }
  }

  /**
   * Событие выбора элемента
   * @param clientEvent
   */
  public void select(ClientEvent clientEvent)
  {
    Map<String, Object> params = clientEvent.getParameters();

    System.out.println("value:" + params.get("param"));
    System.out.println(params.get("param").getClass());

    reset();
    selectByValue(params.get("param"));
  }
}
