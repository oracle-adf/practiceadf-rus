package my.view.java;

import javax.faces.event.ActionEvent;

import my.model.vo.AppModuleImpl;

import my.util.ADFUtils;

import oracle.jbo.Row;
import oracle.jbo.server.ViewObjectImpl;


public class MyBean
{
  public MyBean()
  {
    super();
  }

  private ScriptHost sc = null;
  private AdfBindings adfBindings = null;


  private ScriptHost getScriptHost(){
      AppModuleImpl am = (AppModuleImpl) ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
      if (sc == null)
      {
        // создаем скрипт хост с переменными (AppModuleImpl, AdfBindings)
        adfBindings = new AdfBindings();
        sc = new ScriptHost(am, adfBindings);
      }
      return sc;
  }

  public void runScript(ActionEvent actionEvent)
  {
        Object script = actionEvent.getComponent().getAttributes().get("script");
        Object method = actionEvent.getComponent().getAttributes().get("method");
        System.out.println(script+","+method);
        if(script!=null){
           ScriptHost sh = getScriptHost();           
           sh.invokeMethod((String)script, (String)method);
        }

    }
}
