package my.view.java;

import my.util.ADFUtils;

public class AdfBindings
{
  public AdfBindings()
  {
    super();
  }
  
  
  public Object getAttrValue(String attributeName)
  {
    return ADFUtils.getBoundAttributeValue(attributeName);
  }
  
  public void setAttrValue(String attributeName, Object value)
  {
    ADFUtils.setBoundAttributeValue(attributeName, value);
  }

  
}
