package my.view.java;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import groovy.lang.Script;

import groovy.util.GroovyScriptEngine;

import groovy.util.ResourceException;

import groovy.util.ScriptException;

import java.io.IOException;

import my.model.vo.AppModuleImpl;


public class ScriptHost {
    private AppModuleImpl model;
    private AdfBindings adfBindings;
    private Binding binding;
    private GroovyShell shell;
    private GroovyScriptEngine gse;

    private String[] roots = new String[] { "C:\\Temp\\Script" };

    public ScriptHost(AppModuleImpl model, AdfBindings adfBindings) {
        super();
        this.model = model;
        this.adfBindings = adfBindings;
        binding = new Binding();
        // глобальная переменная - model
        binding.setVariable("model", this.model);
        // глобальная переменная - bindings
        binding.setVariable("bindings", this.adfBindings);
        shell = new GroovyShell(binding);

        try {
            gse = new GroovyScriptEngine(roots);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void evalScript(String script) {
        shell.evaluate(script);
    }

    public void invokeMethod(String script, String method) {
        try {
            Script createScript = gse.createScript(script, binding);
            Object[] args = { };
            createScript.invokeMethod(method, args);        
            System.out.println(script+","+method);
        } catch (ResourceException e) {
            System.out.println(e);
        } catch (ScriptException e) {
            System.out.println(e);
        }
    }

}
