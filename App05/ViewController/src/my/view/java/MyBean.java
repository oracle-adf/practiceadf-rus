package my.view.java;

import javax.faces.event.ActionEvent;

import my.util.ADFUtils;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.ViewObject;

public class MyBean {
    private String jobId;
    private String filterResult;
    private String filterValue;

    public MyBean() {
        super();
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getFilterResult() {
        return filterResult;
    }

    public void setFilterResult(String filterResult) {
        this.filterResult = filterResult;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    /**
     * Добавить запись (Java)
     * @param actionEvent
     */
    public void acAdd(ActionEvent actionEvent) {
        ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject vo = am.findViewObject("JobsView1");
        Row row = vo.createRow();
        row.setAttribute("JobTitle", "Job Title");
        vo.insertRow(row);
    }

    /**
     * Добавить запись (Java + декларативный)
     * @param actionEvent
     */
    public void acAdd3(ActionEvent actionEvent) {
        ADFUtils.findOperation("CreateInsert").execute();
    }

    /**
     * Удаление записи
     * @param actionEvent
     */
    public void acDel(ActionEvent actionEvent) {
        ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject vo = am.findViewObject("JobsView1");
        Row row = vo.getCurrentRow();
        row.remove();
    }

    /**
     * Поиск записи
     * @param actionEvent
     */
    public void acFind(ActionEvent actionEvent) {
        ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject vo = am.findViewObject("JobsView1");
        Row[] byKey = vo.findByKey(new Key(new Object[] { getJobId() }), 1);
        if (byKey != null && byKey.length > 0) {
            vo.setCurrentRow(byKey[0]);
        }

    }

    public void acFilter(ActionEvent actionEvent) {
        ApplicationModule am = ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject vo = am.findViewObject("JobsView1");
        ViewCriteria vc = vo.createViewCriteria();
        ViewCriteriaRow vcr = vc.createViewCriteriaRow();
        vcr.setAttribute("MinSalary", ">=" + getFilterValue());       
        vc.add(vcr);
        vc.setCriteriaMode(ViewCriteria.CRITERIA_MODE_CACHE);
        RowSet rs = (RowSet)vo.findByViewCriteria(vc, -1, ViewObject.QUERY_MODE_SCAN_VIEW_ROWS);
        if ((rs != null) && (rs.getRowCount() > 0)) {
            StringBuilder sb = new StringBuilder();            
            while (rs.hasNext()) {
                Row row = rs.next();
                sb.append(String.format("JobTitle %s, MinSalary %s \n", row.getAttribute("JobTitle"),
                                        row.getAttribute("MinSalary")));                                

            }
            setFilterResult(sb.toString());
        }

    }
}
