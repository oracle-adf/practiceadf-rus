package shell.view.java;

import java.util.ArrayList;
import java.util.List;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.controller.binding.TaskFlowBindingAttributes;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import shell.view.java.utils.ADFUtils;

public class MultiBean {
    private List<TaskFlowBindingAttributes> mTaskFlowBindingAttrs = new ArrayList<TaskFlowBindingAttributes>(5);

    public MultiBean() {

    }

    public List<TaskFlowBindingAttributes> getTaskFlowList() {
        return mTaskFlowBindingAttrs;
    }

    public String initList() {
        ViewObject vo = ADFUtils.findIterator("RegisterVOIterator").getViewObject();                
        vo.executeQuery();        
        Row row = vo.first();        
        while(row!=null){        
            TaskFlowBindingAttributes tfAttr = new TaskFlowBindingAttributes();
            tfAttr.setId((String)row.getAttribute("Name"));
            tfAttr.setTaskFlowId(new TaskFlowId((String)row.getAttribute("Path"), (String)row.getAttribute("Id")));
            mTaskFlowBindingAttrs.add(tfAttr);                    
            row = vo.next();
        }
        
        return null;
    }

}

