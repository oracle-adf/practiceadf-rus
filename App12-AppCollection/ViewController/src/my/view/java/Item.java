package my.view.java;

public class Item {
    public Item() {
        super();
    }
    
    private Integer IdItem;
    private String firstName;
    private String lastName;

    public void setIdItem(Integer IdItem) {
        this.IdItem = IdItem;
    }

    public Integer getIdItem() {
        return IdItem;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }
}
