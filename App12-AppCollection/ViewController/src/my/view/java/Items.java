package my.view.java;

import java.util.ArrayList;
import java.util.List;

public class Items {
    public Items() {
        super();
    }
    
    private List<Item> items = null;
    
    public List<Item> getItems(){
        if(items==null){
            items = new ArrayList<Item>();
        }
        return items;
    }
    
    public Integer getCount(){
        return getItems().size();
    }
}
