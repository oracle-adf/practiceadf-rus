package my.view.java;

import java.util.List;

public class DataControl {
    public DataControl() {
        super();
    }
    
    private Items items= null;
    
    public List<Item> getItems(){
        if (items==null){
            items = new Items();
        }
        return items.getItems();
    }
    
    public Integer getCount(){
        return getItems().size();
    }

}
