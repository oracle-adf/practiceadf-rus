package my.view.java;

import javax.faces.event.ActionEvent;

import my.util.ADFUtils;

import my.util.JSFUtils;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.jbo.AttributeDef;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

public class MyBean {
    public MyBean() {
        super();
    }

    public void testItem(ActionEvent actionEvent) {
        DCIteratorBinding itr = ADFUtils.findIterator("itemsIterator");
        Row row = itr.getViewObject().getCurrentRow();       
        JSFUtils.addFacesInformationMessage(String.format("Id %s, firstName %s, lastName %s", row.getAttribute("idItem"),
                                                          row.getAttribute("firstName"),
                                                          row.getAttribute("lastName")));
    }
}
